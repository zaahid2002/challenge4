package com.zaahid.challenge4.di

import android.content.Context
import com.zaahid.challenge4.data.local.database.AppDataBase
import com.zaahid.challenge4.data.local.database.dao.NoteDao
import com.zaahid.challenge4.data.local.database.dao.UserDao
import com.zaahid.challenge4.data.local.database.datasource.NoteDataSource
import com.zaahid.challenge4.data.local.database.datasource.NoteDataSourceImpl
import com.zaahid.challenge4.data.local.database.datasource.UserDataSource
import com.zaahid.challenge4.data.local.database.datasource.UserDataSourceImpl
import com.zaahid.challenge4.data.local.preference.UserPreference
import com.zaahid.challenge4.data.local.preference.UserPreferenceDataSource
import com.zaahid.challenge4.data.local.preference.UserPreferenceDataSourceImpl
import com.zaahid.challenge4.data.repository.LocalRepository
import com.zaahid.challenge4.data.repository.LocalRepositoryImpl

object ServiceLocator {
    fun provideUserPreference(context: Context): UserPreference {
        return UserPreference(context)
    }

    fun provideUserPreferenceDataSource(context: Context): UserPreferenceDataSource {
        return UserPreferenceDataSourceImpl(provideUserPreference(context))
    }

    fun provideAppDatabase(appContext: Context): AppDataBase {
        return AppDataBase.getInstance(appContext)
    }

    fun provideUserDao(appContext: Context): UserDao {
        return provideAppDatabase(appContext).userDao()
    }

    fun provideUserDataSource(appContext: Context): UserDataSource {
        return UserDataSourceImpl(provideUserDao(appContext))
    }
    fun provideNoteDao(appContext: Context):NoteDao{
        return provideAppDatabase(appContext).noteDao()
    }
    fun provideNoteDataSource(appContext: Context) : NoteDataSource{
        return NoteDataSourceImpl(provideNoteDao(appContext))
    }

    fun provideLocalRepository(context: Context): LocalRepository {
        return LocalRepositoryImpl(
            provideUserPreferenceDataSource(context),
            provideUserDataSource(context),
            provideNoteDataSource(context)
        )
    }
}