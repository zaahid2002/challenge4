package com.zaahid.challenge4.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.zaahid.challenge4.data.local.database.entity.NoteEntity
import com.zaahid.challenge4.databinding.FragmentFormNoteBinding
import com.zaahid.challenge4.di.ServiceLocator
import com.zaahid.challenge4.ui.notelist.NoteViewModel
import com.zaahid.challenge4.utils.viewModelFactory
import com.zaahid.challenge4.wrapper.Resource

class FormNote : DialogFragment() {


    private lateinit var binding :FragmentFormNoteBinding

    private var noteId : Int? = -1
    private var username :String =""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFormNoteBinding.inflate(layoutInflater)
        return binding.root
    }
    private val viewModel : NoteViewModel by viewModelFactory {
        NoteViewModel(ServiceLocator.provideLocalRepository(binding.root.context))
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        username = viewModel.getUserKey()
        if (tag.isNullOrEmpty()){

        }else {
            if (tag == "insert") {
                username

            } else {
                noteId = tag?.toInt()
            }
            observeData()
            setOnClickListener()
            getInitialData()
        }
    }

    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.40).toInt()
        dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
    fun setOnClickListener(){
        binding.buttonsave.setOnClickListener {
            saveData()
        }
    }
    private fun observeData() {
        viewModel.detailDataResult.observe(this) {
            when (it) {
                is Resource.Loading -> {
                    //do nothing
                    setLoading(true)
                }
                is Resource.Success -> {
                    setLoading(false)
                    bindDataToForm(it.payload)
                }
                is Resource.Error -> {
                    setLoading(false)
                    Toast.makeText(context, "Error when get data", Toast.LENGTH_SHORT)
                }
            }
        }
        viewModel.updateResult.observe(this) {
            when (it) {
                is Resource.Loading -> {
                    setFormEnabled(false)
                    setLoading(true)
                }
                is Resource.Success -> {
                    setFormEnabled(true)
                    setLoading(false)

                    Toast.makeText(context, "Update data Success", Toast.LENGTH_SHORT).show()
                    dismiss()
                }
                is Resource.Error -> {
                    setFormEnabled(true)
                    setLoading(false)
                    Toast.makeText(context, "Error when get data", Toast.LENGTH_SHORT).show()
                }
            }
        }
        viewModel.insertResult.observe(this) {
            when (it) {
                is Resource.Loading -> {
                    //do nothing
                    setFormEnabled(false)
                    setLoading(true)
                }
                is Resource.Success -> {
                    setFormEnabled(true)
                    setLoading(false)
                    Toast.makeText(context, "Add New data Success", Toast.LENGTH_SHORT).show()
                    dismiss()
                }
                is Resource.Error -> {
                    setFormEnabled(true)
                    setLoading(false)
                    Toast.makeText(context, "Error when get data", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    private fun setLoading(isLoading: Boolean) {
        binding.pbNoteForm.isVisible = isLoading
        binding.clForm.isVisible = !isLoading
    }

    private fun setFormEnabled(isFormEnabled: Boolean) {
        with(binding) {
            inputtextnotename.isEnabled = isFormEnabled
            inputtextvalue.isEnabled = isFormEnabled
        }
    }


    private fun bindDataToForm(data: NoteEntity?) {
        data?.let {
            binding.inputtextnotename.setText(data.notename)
            binding.inputtextvalue.setText(data.notevalue)
        }
    }

    private fun parseFormIntoEntity(): NoteEntity {
        return NoteEntity(
            nameuser = username,
            notename = binding.inputtextnotename.text.toString().trim(),
            notevalue = binding.inputtextvalue.text.toString().trim(),

        ).apply {
            if (isEditAction()) {
                noteId?.let {
                    idnote = it
                }
            }
        }
    }
    private fun isEditAction(): Boolean {
        val a : Int = -1
        return noteId != null && noteId != a
    }
    fun checkForm():Boolean{
        val notename = binding.inputtextnotename.text.toString()
        val notevalue = binding.inputtextvalue.text.toString()
        var isFormValid = true
        if (notename.isEmpty()){
            isFormValid = false
            binding.layoutinputnama.isErrorEnabled = true
            binding.layoutinputnama.error = "empty name"
        }else binding.layoutinputnama.isErrorEnabled = false

        if (notevalue.isEmpty()){
            isFormValid=false
            binding.layoutvalue.isErrorEnabled =true
            binding.layoutvalue.error = "Empty value"
        }else binding.layoutvalue.isErrorEnabled=false
        return isFormValid
    }
    private fun getInitialData() {
        if (isEditAction()) {
            noteId?.let {
                viewModel.getNoteById(it)
            }
        }
    }
    private fun saveData() {
        if (checkForm()) {
            if (isEditAction()) {
                viewModel.updateNote(parseFormIntoEntity())
            } else {
                viewModel.insertNote(parseFormIntoEntity())
            }
        }
    }


}