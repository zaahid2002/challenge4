package com.zaahid.challenge4.ui.formregister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.zaahid.challenge4.R
import com.zaahid.challenge4.data.local.database.entity.UserEntity
import com.zaahid.challenge4.databinding.ActivityFormRegisterBinding
import com.zaahid.challenge4.di.ServiceLocator
import com.zaahid.challenge4.ui.MainActivity
import com.zaahid.challenge4.ui.UserViewModel
import com.zaahid.challenge4.ui.notelist.NoteList
import com.zaahid.challenge4.utils.viewModelFactory

class FormRegister : AppCompatActivity() {
    private val binding: ActivityFormRegisterBinding by lazy {
        ActivityFormRegisterBinding.inflate(layoutInflater)
    }
    private val viewmodel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(this))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.button.setOnClickListener { cekForm() }
    }
    fun cekForm(){
        if (binding.textinputusername.text.toString().isEmpty().not()){
            if (binding.textinputpassword.text.toString().isEmpty().not()){
                if (binding.inputConfirmPassword.text.toString().isEmpty().not()){
                    register()
                }else Toast.makeText(this,"confirm password belum di isi",Toast.LENGTH_SHORT).show()
            }else Toast.makeText(this,"password belum di isi",Toast.LENGTH_SHORT).show()
        }else Toast.makeText(this,"username belum di isi",Toast.LENGTH_SHORT).show()
    }

    fun register(){
            if (cekPasswordIsSame()){
                viewmodel.insertUser(parseFormToEntity())
                Toast.makeText(this,"berhasil memasukan data",Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }else Toast.makeText(this,"password tidak sama memasukan data",Toast.LENGTH_LONG).show()

    }
    fun parseFormToEntity() :UserEntity{
        return UserEntity(
            username = binding.textinputusername.text.toString().trim(),
            useremail = binding.textinputemail.text.toString().trim(),
            userpassword = binding.textinputpassword.text.toString().trim()
        )
    }
    fun cekPasswordIsSame() : Boolean{
        return binding.textinputpassword.text.toString().trim() == binding.inputConfirmPassword.text.toString().trim()
    }
}