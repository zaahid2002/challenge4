package com.zaahid.challenge4.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.zaahid.challenge4.databinding.FragmentDeleteBinding
import com.zaahid.challenge4.di.ServiceLocator
import com.zaahid.challenge4.ui.notelist.NoteViewModel
import com.zaahid.challenge4.utils.viewModelFactory
import com.zaahid.challenge4.wrapper.Resource


class DeleteFragment : DialogFragment() {
    private lateinit var binding : FragmentDeleteBinding

    private var noteId : Int? = -1
    private var username :String =""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDeleteBinding.inflate(layoutInflater)
        return binding.root
    }
    private val viewModel : NoteViewModel by viewModelFactory {
        NoteViewModel(ServiceLocator.provideLocalRepository(binding.root.context))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noteId = tag?.toInt()
        username = viewModel.getUserKey()
        val picknote = noteId?.let { viewModel.getNoteById(it) }
        binding.buttondelete.setOnClickListener {
            viewModel.detailDataResult.observe(this) {
                when (it) {
                    is Resource.Loading -> {
                        //do nothing
                        setLoading(true)
                    }
                    is Resource.Success -> {
                        setLoading(false)
                        it.payload?.let { it1 -> viewModel.deleteNote(it1) }
                        viewModel.getNoteUser(username)
                        dismiss()
                    }
                    is Resource.Error -> {
                        setLoading(false)
                        Toast.makeText(context, "Error when get data", Toast.LENGTH_SHORT)
                    }
                }
            }
        }
        binding.buttoncancel.setOnClickListener {
            dismiss()
        }
    }
    private fun setLoading(isLoading: Boolean) {
        binding.pbNoteForm.isVisible = isLoading
        binding.clForm.isVisible = !isLoading
    }




}