package com.zaahid.challenge4.ui.notelist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zaahid.challenge4.data.local.database.entity.NoteEntity
import com.zaahid.challenge4.data.repository.LocalRepository
import com.zaahid.challenge4.wrapper.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class NoteViewModel(private val repository: LocalRepository) :ViewModel() {
    val detailDataResult = MutableLiveData<Resource<NoteEntity>>()
    val noteListResult = MutableLiveData<Resource<List<NoteEntity>>>()
    val userNote = getUserKey()+"'s Note"
    val deleteResult = MutableLiveData<Resource<Number>>()
    val updateResult = MutableLiveData<Resource<Number>>()
    val insertResult = MutableLiveData<Resource<Number>>()

    fun checkIfUserKeyIsExist(): Boolean {
        return repository.checkIfUserKeyIsExist()
    }

    fun getUserKey() :String{
        return repository.getUserKey()
    }
    fun setUserKey(key : String) {
        repository.setUserKey(key)
    }

    fun getNoteUser(username :String){
        viewModelScope.launch {
            noteListResult.postValue(Resource.Loading())
            delay(1000)
            noteListResult.postValue(repository.getNoteUser(username))
        }
    }
    fun getNoteById(id : Int){
        viewModelScope.launch {
            detailDataResult.postValue(Resource.Loading())
            delay(1000)
            detailDataResult.postValue(repository.getNoteById(id))
        }
    }

    fun deleteNote(item: NoteEntity) {
        viewModelScope.launch {
            deleteResult.postValue(Resource.Loading())
            delay(1000)
            deleteResult.postValue(repository.deleteNote(item))
        }
    }
    fun updateNote(item: NoteEntity){
        viewModelScope.launch {
            updateResult.postValue(Resource.Loading())
            delay(1000)
            updateResult.postValue(repository.updateNote(item))
        }
    }
    fun insertNote(item: NoteEntity){
        viewModelScope.launch {
            insertResult.postValue(Resource.Loading())
            delay(1000)
            insertResult.postValue(repository.insertNote(item))
        }
    }
}