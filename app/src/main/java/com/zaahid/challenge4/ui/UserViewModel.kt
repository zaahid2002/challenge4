package com.zaahid.challenge4.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zaahid.challenge4.data.local.database.entity.UserEntity
import com.zaahid.challenge4.data.repository.LocalRepository
import com.zaahid.challenge4.wrapper.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UserViewModel(private val repository: LocalRepository) : ViewModel() {
    val detailDataResult = MutableLiveData<Resource<UserEntity?>>()
    val insertResult = MutableLiveData<Resource<Number>>()

    fun insertUser(userEntity: UserEntity){
        viewModelScope.launch {
            insertResult.postValue(Resource.Loading())
            delay(1000)
            insertResult.postValue(repository.insertUser(userEntity))
        }
    }
    fun getUserByUsername(username : String) {
        viewModelScope.launch {
            detailDataResult.postValue(Resource.Loading())
            delay(1000)
            detailDataResult.postValue(repository.getUserByUsername(username))
        }
    }
    fun checkIfUserKeyIsExist() : Boolean{
        return repository.checkIfUserKeyIsExist()
    }
    fun setUserKey(user :String){
        repository.setUserKey(user)
    }
    fun getUserKey():String{
        return repository.getUserKey()
    }

}