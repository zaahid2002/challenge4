package com.zaahid.challenge4.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.zaahid.challenge4.data.local.database.entity.UserEntity
import com.zaahid.challenge4.databinding.ActivityMainBinding
import com.zaahid.challenge4.di.ServiceLocator
import com.zaahid.challenge4.ui.formregister.FormRegister
import com.zaahid.challenge4.ui.notelist.NoteList
import com.zaahid.challenge4.utils.viewModelFactory

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val viewModel : UserViewModel by viewModelFactory {
        UserViewModel(ServiceLocator.provideLocalRepository(this))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toRegister()

        binding.buttonLogin.setOnClickListener {
            cekForm()
        }
    }

    override fun onResume() {
        super.onResume()
//        cekstatus()
    }
    fun toRegister(){
        binding.toregister.setOnClickListener {
            val intent = Intent(this@MainActivity,FormRegister::class.java)
            startActivity(intent)
        }
    }
    fun cekUserKey() :Boolean{
        return viewModel.checkIfUserKeyIsExist()
    }
    fun cekstatus(){
        if (cekUserKey()){
            val username = viewModel.getUserKey()
            Toast.makeText(this,username+" berhasil masuk",Toast.LENGTH_SHORT).show()
            val intent = Intent(this,NoteList::class.java)
            startActivity(intent)
        }
    }
    fun cekForm(){
        if (binding.inputusename.text.toString().isEmpty().not()){
            if (binding.textinputpassword.text.toString().isEmpty().not()){
                viewModel.getUserByUsername(binding.inputusename.text.toString().trim())
                viewModel.detailDataResult.observe(this){
                    login(it.payload)
                }
            }else Toast.makeText(this,"password belum di isi",Toast.LENGTH_SHORT).show()
        }else Toast.makeText(this,"username belum di isi", Toast.LENGTH_SHORT).show()
    }
    fun login(user : UserEntity?){
        if (user == null){
            Toast.makeText(this,"username yang di masukan salah",Toast.LENGTH_SHORT).show()
        }else{
            if (user.username.toString().isEmpty().not()){
                viewModel.setUserKey(user.username.toString())
                Toast.makeText(this,user.username+" berhasil masuk",Toast.LENGTH_SHORT).show()
                val intent = Intent(this,NoteList::class.java)
                startActivity(intent)
            }else Toast.makeText(this,"gagalmasuk",Toast.LENGTH_SHORT).show()
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}