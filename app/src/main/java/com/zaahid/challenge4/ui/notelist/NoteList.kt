package com.zaahid.challenge4.ui.notelist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.zaahid.challenge4.R
import com.zaahid.challenge4.data.local.database.entity.NoteEntity
import com.zaahid.challenge4.databinding.ActivityNoteListBinding
import com.zaahid.challenge4.di.ServiceLocator
import com.zaahid.challenge4.ui.MainActivity
import com.zaahid.challenge4.ui.fragment.DeleteFragment
import com.zaahid.challenge4.ui.fragment.FormNote
import com.zaahid.challenge4.utils.viewModelFactory
import com.zaahid.challenge4.wrapper.Resource

class NoteList : AppCompatActivity() {
    private val viewModelNote: NoteViewModel by viewModelFactory {
        NoteViewModel(ServiceLocator.provideLocalRepository(this))
    }
    lateinit var username :String
    private val adapter : NoteListAdapter by lazy {
        NoteListAdapter(object : NoteItemClickListener{
            override fun onEditClicked(item: NoteEntity) {
                createDialogUpdate(item.idnote)
            }

            override fun onDeleteClicked(item: NoteEntity) {
                viewModelNote.deleteNote(item)
            }

        })
    }
    private lateinit var binding: ActivityNoteListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = DataBindingUtil.setContentView(this, R.layout.activity_note_list)

        binding.viewmodel = viewModelNote
        binding.lifecycleOwner = this
        initList()
        setOnClickListeners()
        observeData()
        username = viewModelNote.getUserKey().toString()
        binding.buttonlogout.setOnClickListener {
            viewModelNote.setUserKey("")
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        binding.refersh.setOnClickListener {
            viewModelNote.getNoteUser(username)
        }
    }

    private fun initList() {
        binding.rvNoteList .apply {
            layoutManager = LinearLayoutManager(this@NoteList)
            adapter = this@NoteList.adapter
        }
    }
    private fun setOnClickListeners() {
        binding.fabAddData.setOnClickListener {
            createDialogInsert()
        }
    }

    private fun observeData() {
        viewModelNote.noteListResult.observe(this) {
            when (it) {
                is Resource.Loading -> {
                    setLoadingState(true)
                    setErrorState(false)
                }
                is Resource.Success -> {
                    setLoadingState(false)
                    setErrorState(false)
                    bindDataToAdapter(it.payload)
                }
                is Resource.Error -> {
                    setLoadingState(false)
                    setErrorState(true, it.exception?.message.orEmpty())
                }
            }
        }
        viewModelNote.deleteResult.observe(this) {
            when (it) {
                is Resource.Loading -> {

                }
                is Resource.Success -> {
                    Toast.makeText(this, "Delete Password Success", Toast.LENGTH_SHORT).show()
                    getData(username)
                }
                is Resource.Error -> {
                    Toast.makeText(this, "Delete Password Failed", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    private fun setErrorState(isError: Boolean, errorMsg: String = "") {
        binding.tvError.text = errorMsg
        binding.tvError.isVisible = isError
    }
    private fun setLoadingState(isLoading: Boolean) {
        binding.pbNoteList.isVisible = isLoading
        binding.rvNoteList.isVisible = !isLoading
    }
    private fun getData(username : String) {
        viewModelNote.getNoteUser(username)
    }
    private fun bindDataToAdapter(data: List<NoteEntity>?) {
        if (data.isNullOrEmpty()) {
            adapter.clearItems()
            setErrorState(true,"your Note is empty" )
        } else {
            adapter.setItems(data)
        }
    }
    override fun onResume() {
        super.onResume()
        getData(username)

    }
    fun createDialogInsert(){
        val fragmentManager = supportFragmentManager
        val newFragment = FormNote()
        newFragment.show(fragmentManager,"insert")
        viewModelNote.getNoteUser(username)
    }
    fun createDialogUpdate(id : Int){
        val fragmentManager = supportFragmentManager
        val newFragment = FormNote()
        newFragment.show(fragmentManager,id.toString())
        viewModelNote.getNoteUser(username)
    }
    fun createDialogDelete(item : NoteEntity){
        val fragmentManager = supportFragmentManager
        val newFragment = DeleteFragment()
        newFragment.show(fragmentManager,item.idnote.toString())
        viewModelNote.getNoteUser(username)
    }
}


