package com.zaahid.challenge4.ui.notelist

import android.app.SearchManager
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.zaahid.challenge4.R
import com.zaahid.challenge4.data.local.database.entity.NoteEntity
import com.zaahid.challenge4.databinding.ItemNoteBinding

class NoteListAdapter(
    private val listener: NoteItemClickListener
) :
    RecyclerView.Adapter<NoteListAdapter.NoteListViewHolder>() {
    private var items: MutableList<NoteEntity> = mutableListOf()
    override fun getItemCount(): Int = items.size
    fun setItems(items: List<NoteEntity>) {
        clearItems()
        addItems(items)
        notifyDataSetChanged()
    }

    fun addItems(items: List<NoteEntity>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.items.clear()
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteListViewHolder {
        val binding =
            ItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoteListViewHolder(binding,listener)
    }

    override fun onBindViewHolder(holder: NoteListViewHolder, position: Int) {
        holder.bindView(items[position])
    }

    class NoteListViewHolder(
        private val binding: ItemNoteBinding,
        private val listener: NoteItemClickListener
        ): RecyclerView.ViewHolder(binding.root){
        fun bindView(item : NoteEntity){
            with(item){
                binding.tvNotename.text = this.notename.toString()
                binding.tvNotevalue.text = this.notevalue.toString()
                binding.buttonedit.setOnClickListener { listener.onEditClicked(this) }
                binding.buttontrash.setOnClickListener { listener.onDeleteClicked(this) }
            }
        }
    }
}
interface NoteItemClickListener {
    fun onEditClicked(item: NoteEntity)
    fun onDeleteClicked(item: NoteEntity)
}