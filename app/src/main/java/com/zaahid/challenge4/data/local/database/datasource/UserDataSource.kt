package com.zaahid.challenge4.data.local.database.datasource

import com.zaahid.challenge4.data.local.database.dao.UserDao
import com.zaahid.challenge4.data.local.database.entity.UserEntity

interface UserDataSource {
    suspend fun getUserByusername(id: String): UserEntity?

    suspend fun insertUser(password: UserEntity): Long
}

class UserDataSourceImpl(private val userDao: UserDao) : UserDataSource {

    override suspend fun getUserByusername(username: String): UserEntity {
        return userDao.getUserByusername(username)
    }

    override suspend fun insertUser(password: UserEntity): Long {
        return userDao.insertUser(password)
    }

}