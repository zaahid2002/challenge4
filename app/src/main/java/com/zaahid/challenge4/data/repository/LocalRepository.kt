package com.zaahid.challenge4.data.repository

import com.zaahid.challenge4.data.local.database.datasource.NoteDataSource
import com.zaahid.challenge4.data.local.database.datasource.UserDataSource
import com.zaahid.challenge4.data.local.database.entity.NoteEntity
import com.zaahid.challenge4.data.local.database.entity.UserEntity
import com.zaahid.challenge4.data.local.preference.UserPreferenceDataSource
import com.zaahid.challenge4.wrapper.Resource

interface LocalRepository {
    fun checkIfUserKeyIsExist(): Boolean
    fun setUserKey(newAppKey: String)
    fun getUserKey() :String
    suspend fun getUserByUsername(username : String): Resource<UserEntity?>
    suspend fun insertUser(user : UserEntity): Resource<Number>
    suspend fun getNoteList(): Resource<List<NoteEntity>>
    suspend fun getNoteById(id: Int): Resource<NoteEntity>
    suspend fun getNoteUser(username: String) : Resource<List<NoteEntity>>
    suspend fun insertNote(noteEntity: NoteEntity): Resource<Number>
    suspend fun deleteNote(noteEntity: NoteEntity): Resource<Number>
    suspend fun updateNote(noteEntity: NoteEntity): Resource<Number>
}

class LocalRepositoryImpl(
    private val userPreferenceDataSource: UserPreferenceDataSource,
    private val userDataSource: UserDataSource,
    private val noteDataSource: NoteDataSource
) : LocalRepository {

    override fun checkIfUserKeyIsExist(): Boolean {
        return userPreferenceDataSource.getUserKey().isNullOrEmpty().not()
    }

    override fun setUserKey(newAppKey: String) {
        userPreferenceDataSource.setUserKey(newAppKey)
    }

    override fun getUserKey(): String {
        return userPreferenceDataSource.getUserKey().toString()
    }

    override suspend fun getUserByUsername(username: String): Resource<UserEntity?> {
        return proceed {
            userDataSource.getUserByusername(username)
        }
    }

    override suspend fun insertUser(user: UserEntity): Resource<Number> {
        return proceed {
            userDataSource.insertUser(user)
        }
    }

    override suspend fun getNoteList(): Resource<List<NoteEntity>> {
        return proceed {
            noteDataSource.getAllNote()
        }
    }

    override suspend fun getNoteById(id: Int): Resource<NoteEntity> {
        return proceed {
            noteDataSource.getNoteById(id)
        }
    }

    override suspend fun getNoteUser(username: String): Resource<List<NoteEntity>> {
        return proceed {
            noteDataSource.getNoteUser(username)
        }
    }

    override suspend fun insertNote(noteEntity: NoteEntity): Resource<Number> {
        return proceed {
            noteDataSource.insertNote(noteEntity)
        }
    }

    override suspend fun deleteNote(noteEntity: NoteEntity): Resource<Number> {
        return proceed {
            noteDataSource.deleteNote(noteEntity)
        }
    }

    override suspend fun updateNote(noteEntity: NoteEntity): Resource<Number> {
        return proceed {
            noteDataSource.updateNote(noteEntity)
        }
    }

    private suspend fun <T> proceed(coroutine: suspend () -> T): Resource<T> {
        return try {
            Resource.Success(coroutine.invoke())
        } catch (exception: Exception) {
            Resource.Error(exception)
        }
    }

}