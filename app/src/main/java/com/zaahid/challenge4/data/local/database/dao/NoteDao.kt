package com.zaahid.challenge4.data.local.database.dao

import androidx.room.*
import com.zaahid.challenge4.data.local.database.entity.NoteEntity


@Dao
interface NoteDao {
    @Query("SELECT * FROM NOTE")
    suspend fun getAllNote() : List<NoteEntity>

    @Query("SELECT * FROM NOTE WHERE IDNOTE == :id LIMIT 1")
    suspend fun getNoteById(id:Int) : NoteEntity

    @Query("SELECT *FROM NOTE WHERE USERID == :username ")
    suspend fun getNoteUser(username :String) :List<NoteEntity>

    @Insert
    suspend fun insertNote(noteEntity: NoteEntity) : Long
    @Delete
    suspend fun deleteNote(noteEntity: NoteEntity) :Int
    @Update
    suspend fun updateNote(noteEntity: NoteEntity) : Int
}