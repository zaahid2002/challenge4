package com.zaahid.challenge4.data.local.database.dao

import androidx.room.*
import com.zaahid.challenge4.data.local.database.entity.UserEntity


@Dao
interface UserDao {
    @Query("SELECT * FROM USER WHERE USERNAME == :username LIMIT 1")
    suspend fun getUserByusername(username : String) :UserEntity

    @Insert
    suspend fun insertUser(user : UserEntity) : Long
}