package com.zaahid.challenge4.data.local.database.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "user")
class UserEntity(
    @PrimaryKey(autoGenerate = false)
    var username : String,
    @ColumnInfo(name = "useremail")
    var useremail : String?,
    @ColumnInfo(name = "userpassword")
    var userpassword : String?
) :Parcelable