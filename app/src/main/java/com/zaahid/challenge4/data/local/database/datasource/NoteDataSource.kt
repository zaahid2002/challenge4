package com.zaahid.challenge4.data.local.database.datasource

import com.zaahid.challenge4.data.local.database.dao.NoteDao
import com.zaahid.challenge4.data.local.database.entity.NoteEntity


interface NoteDataSource {
    suspend fun getAllNote(): List<NoteEntity>
    suspend fun getNoteUser(username : String) :List<NoteEntity>

    suspend fun getNoteById(id: Int): NoteEntity

    suspend fun insertNote(note: NoteEntity): Long

    suspend fun deleteNote(note: NoteEntity): Int

    suspend fun updateNote(note: NoteEntity): Int
}

class NoteDataSourceImpl(private val noteDao: NoteDao) : NoteDataSource {
    override suspend fun getAllNote(): List<NoteEntity> {
        return noteDao.getAllNote()
    }

    override suspend fun getNoteUser(username: String): List<NoteEntity> {
        return noteDao.getNoteUser(username)
    }

    override suspend fun getNoteById(id: Int): NoteEntity {
        return noteDao.getNoteById(id)
    }

    override suspend fun insertNote(note: NoteEntity): Long {
        return noteDao.insertNote(note)
    }

    override suspend fun deleteNote(note: NoteEntity): Int {
        return noteDao.deleteNote(note)
    }

    override suspend fun updateNote(note: NoteEntity): Int {
        return noteDao.updateNote(note)
    }

}