package com.zaahid.challenge4.data.local.database.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = "note")
class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    var idnote : Int = 0,
    @ColumnInfo(name = "userid")
    var nameuser : String,
    @ColumnInfo(name = "notename")
    var notename : String?,
    @ColumnInfo(name = "notevalue")
    var notevalue : String?
) :Parcelable