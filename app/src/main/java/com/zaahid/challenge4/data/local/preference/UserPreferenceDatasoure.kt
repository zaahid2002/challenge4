package com.zaahid.challenge4.data.local.preference

interface UserPreferenceDataSource {
    fun getUserKey() : String?
    fun setUserKey(newAppKey:String)
}
class UserPreferenceDataSourceImpl(
    private val userPreference: UserPreference
) : UserPreferenceDataSource    {
    override fun getUserKey(): String? {
        return userPreference.keyuser
    }
    override fun setUserKey(newUserKey: String) {
        userPreference.keyuser = newUserKey
    }
}